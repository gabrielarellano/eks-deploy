FROM bitnami/aws-cli:2.2.5-debian-10-r6 AS build

ARG CURL_VERSION=7.64.0-4+deb10u2
ARG EKSCTL_VERSION=0.50.0
ARG KUBECTL_VERSION=v1.21.1

WORKDIR /tools

# hadolint ignore=DL3002
USER root

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends curl=$CURL_VERSION \
    && curl -LO "https://github.com/weaveworks/eksctl/releases/download/$EKSCTL_VERSION/eksctl_Linux_amd64.tar.gz"  \
    && tar xzf eksctl_Linux_amd64.tar.gz \
    && mv eksctl /usr/local/bin/ \
    && curl -LO "https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/

FROM bitnami/aws-cli:2.2.5-debian-10-r6

LABEL maintainer="gabrielarellano@gmail.com"

COPY --from=build /usr/local/bin/* /usr/local/bin/

USER root
RUN mkdir /home/aws \
    && chown -R 1001 /home/aws

USER 1001
ENV HOME="/home/aws"
WORKDIR /home/aws

ENTRYPOINT [ "" ]
CMD ["/bin/bash"]

